/usr/bin/time -f "${1},STD,%E" sh -c "./${1}.std 1>/dev/null 2>/dev/null"
/usr/bin/time -f "${1},STD+VALU,%E" sh -c "./${1}.std-valu 1>/dev/null 2>/dev/null"
/usr/bin/time -f "${1},O3,%E" sh -c "./${1}.baseline 1>/dev/null 2>/dev/null"
/usr/bin/time -f "${1},VALU+SLP,%E" sh -c "./${1}.luslp 1>/dev/null 2>/dev/null"
/usr/bin/time -f "${1},LV,%E" sh -c "./${1}.vec 1>/dev/null 2>/dev/null"
/usr/bin/time -f "${1},DU+SLP,%E" sh -c "./${1}.unroll-slp 1>/dev/null 2>/dev/null"
#for Count in 2 4 8; do
#/usr/bin/time -f "${1},U${Count}+SLP,%E" sh -c "./${1}.unroll-${Count}-slp 1>/dev/null 2>/dev/null"
#done
/usr/bin/time -f "${1},SLP,%E" sh -c "./${1}.slp 1>/dev/null 2>/dev/null"

#/usr/bin/time -f "${1},LV+SLP,%E" sh -c "./${1}.lv-slp 1>/dev/null 2>/dev/null"
#/usr/bin/time -f "${1},LV+DU+SLP,%E" sh -c "./${1}.lv-unroll-slp 1>/dev/null 2>/dev/null"
#/usr/bin/time -f "${1},LV+VALU+SLP,%E" sh -c "./${1}.lv-luslp 1>/dev/null 2>/dev/null"
