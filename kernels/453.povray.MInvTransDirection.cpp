#include "common.h"

#define DBL double

typedef DBL UV_VECT[2];
typedef DBL VECTOR[3];
typedef DBL VECTOR_4D[4];
typedef DBL MATRIX[4][4];
typedef DBL EXPRESS[5];
//typedef COLC COLOUR[5];
//typedef COLC RGB[3];
//typedef SNGL SNGL_VECT[3];


typedef struct Transform_Struct TRANSFORM;

struct Transform_Struct
{
  MATRIX matrix;
  MATRIX inverse;
};

/* Vector array elements. */
enum
{
	U = 0,
	V = 1
};

enum
{
	X = 0,
	Y = 1,
	Z = 2,
	T = 3
};


void MInvTransDirection (VECTOR result, VECTOR  vector, TRANSFORM *transform)
{
  register int i;
  DBL answer_array[4];
  MATRIX *matrix;

  matrix = (MATRIX *) transform->inverse;

  for (i = 0 ; i < 3 ; i++)
  {
    answer_array[i] = vector[X] * (*matrix)[0][i] +
                      vector[Y] * (*matrix)[1][i] +
                      vector[Z] * (*matrix)[2][i];
  }

  result[X] = answer_array[0];
  result[Y] = answer_array[1];
  result[Z] = answer_array[2];
}


#define NRep 50000000L
int main() {
  VECTOR result;
  VECTOR vector;
  TRANSFORM transform;
  #pragma nounroll
  for(long repeat = 0; repeat<NRep; repeat++) {
    __escape__(result);
    __escape__(vector);
    __escape__(&transform);
    MInvTransDirection(result, vector, &transform);
    MInvTransDirection(result, vector, &transform);
    MInvTransDirection(result, vector, &transform);
    MInvTransDirection(result, vector, &transform);

    MInvTransDirection(result, vector, &transform);
    MInvTransDirection(result, vector, &transform);
    MInvTransDirection(result, vector, &transform);
    MInvTransDirection(result, vector, &transform);
    __escape__(result);
    __escape__(vector);
    __escape__(&transform);
  }
  return 0;
}

