#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


/* Directions, and a macro to give the opposite direction */
/*  These must go from 0 to 7 because they will be used to index an
    array. */
/* Also define NDIRS = number of directions */
#define XUP 0
#define YUP 1
#define ZUP 2
#define TUP 3
#define TDOWN 4
#define ZDOWN 5
#define YDOWN 6
#define XDOWN 7

#define NODIR -1  /* not a direction */

#define OPP_DIR(dir)    (7-(dir))       /* Opposite direction */
#define NDIRS 8                         /* number of directions */


/* defines NREPS NLOOP MAX_LENGTH MAX_NUM */
#define NREPS 1
#define NLOOP 3
#define MAX_LENGTH 6
#define MAX_NUM 16



#ifdef LOOPEND
#undef FORALLSITES
#define FORALLSITES(i,s) \
{ register int loopend; loopend=sites_on_node; \
for( i=0,  s=lattice ; i<loopend; i++,s++ )
#define END_LOOP }
#else
#define END_LOOP        /* define it to be nothing */
#endif

#define GOES_FORWARDS(dir) (dir<=TUP)
#define GOES_BACKWARDS(dir) (dir>TUP)

//char gauge_action_description[128];
int  gauge_action_nloops=NLOOP;
int  gauge_action_nreps=NREPS;
int loop_length[NLOOP];	/* lengths of various kinds of loops */
int loop_num[NLOOP];	/* number of rotations/reflections  for each kind */

    /* table of directions, 1 for each kind of loop */
int loop_ind[NLOOP][MAX_LENGTH];
    /* table of directions, for each rotation and reflection of each kind of
	loop.  tabulated with "canonical" starting point and direction. */
int loop_table[NLOOP][MAX_NUM][MAX_LENGTH];
    /* table of coefficients in action, for various "representations" (actually,
	powers of the trace) */
double loop_coeff[NLOOP][NREPS];
    /* for each rotation/reflection, an integer distinct for each starting
	point, or each cyclic permutation of the links */
int loop_char[MAX_NUM];
    /* for each kind of loop for each rotation/reflection, the expectation
	value of the loop */
double loop_expect[NLOOP][NREPS][MAX_NUM];



/* find a number uniquely identifying the cyclic permutation of a path,
   or the starting point on the path.  Backwards paths are considered
   equivalent here, so scan those too. */
void char_num( int *dig, int *chr, int length){
    int j;
    int bdig[MAX_LENGTH],tenl,newv,old;
    /* "dig" is array of directions.  "bdig" is array of directions for
        backwards path. */

 
  //int vec[MAX_LENGTH];
  /*
  for (int i = 0; i<MAX_LENGTH; i++) printf("%d, ", dig[i]);
  printf("\n");
  */
  tenl=1;
  for(j=0;j<length-1;j++) tenl=tenl*10;

  *chr=dig[length-1];
  for(j=length-2;j>=0;j--) *chr= *chr*10+dig[j];

  /* forward*/
  old=*chr;
  for(j=length-1;j>=1;j--){
       newv=old-tenl*dig[j];
       newv=newv*10+dig[j];
       if(newv < *chr) *chr=newv;
       old=newv;           }

   /* backward*/
   for(j=0;j<length;j++)bdig[j]=7-dig[length-j-1];
   old=bdig[length-1];
   for(j=length-2;j>=0;j--) old=old*10+bdig[j];
   if(old < *chr ) *chr=old;
   for(j=length-1;j>=1;j--){
       newv=old-tenl*bdig[j];
       newv=newv*10+bdig[j];
       if(newv < *chr) *chr=newv;
       old=newv;           }

} /* char_num */

/* Make table of loops in action */

double u0 = 1.;
void make_loop_table() {

    int perm[8],pp[8],ir[4];
    int length,iloop,i,j,chr;
    int vec[MAX_LENGTH];
    int count,flag;
    void char_num( int *dig, int *chr, int length);


    static int loop_ind[NLOOP][MAX_LENGTH] = {
    { XUP, YUP, XDOWN, YDOWN, NODIR, NODIR },
    { XUP, XUP, YUP, XDOWN, XDOWN , YDOWN},
    { XUP, YUP, ZUP, XDOWN, YDOWN , ZDOWN},
    };
    static int loop_length_in[NLOOP] = {4,6,6};

    for(j=0;j<NLOOP;j++){
        loop_num[j] = 0;
        loop_length[j] = loop_length_in[j];
        for(i=0;i<NREPS;i++){
            loop_coeff[j][i] = 0.0;
        }
    }

    // * Loop coefficients from Urs 
    loop_coeff[0][0]= 1.0;
    loop_coeff[1][0]=  -1.00/(20.0*u0*u0) * (1.00 - 0.6264*log(u0) );
    loop_coeff[2][0]=  1.00/(u0*u0) * 0.04335 * log(u0);
    //strcpy(gauge_action_description,"\"Symanzik 1x1 + 1x2 + 1x1x1 action\"");
    //node0_printf("Symanzik 1x1 + 1x2 + 1x1x1 action\n");

    for(iloop=0;iloop<NLOOP;iloop++){
        length=loop_length[iloop];
        count=0;
        // * permutations 
        for(perm[0]=0;perm[0]<4;perm[0]++)
        for(perm[1]=0;perm[1]<4;perm[1]++)
        for(perm[2]=0;perm[2]<4;perm[2]++)
        for(perm[3]=0;perm[3]<4;perm[3]++){
            if(perm[0] != perm[1] && perm[0] != perm[2]
                && perm[0] != perm[3] && perm[1] != perm[2]
                && perm[1] != perm[3] && perm[2] != perm[3] ) {
                // * reflections
                for(ir[0]=0;ir[0]<2;ir[0]++)
                for(ir[1]=0;ir[1]<2;ir[1]++)
                for(ir[2]=0;ir[2]<2;ir[2]++)
                for(ir[3]=0;ir[3]<2;ir[3]++){
                    for(j=0;j<4;j++){
                        pp[j]=perm[j];

                        if(ir[j] == 1) pp[j]=7-pp[j];
                        pp[7-j]=7-pp[j];
                    }
                    // * create new vector
                    for(j=0;j<length;j++) vec[j]=pp[loop_ind[iloop][j]];

                    char_num(vec,&chr,length);
                    flag=0;
                    // * check if it's a new set:
                    for(j=0;j<count;j++) if(chr == loop_char[j])flag=1;
                    if(flag == 0 ){
                        loop_char[count]=chr;
                        for(j=0;j<length;j++)
                            loop_table[iloop][count][j]=vec[j];
                        count++;
// **node0_printf("ADD LOOP: "); printpath( vec, length );
                    }
                    if(count>MAX_NUM){
                        //node0_printf("OOPS: MAX_NUM too small\n");
                        exit(0);
                    }
                    loop_num[iloop]=count;

                } // * end reflection
            } // * end permutation if block
        } // * end permutation
    } // * end iloop

    // * print out the loop coefficients
/*
    node0_printf("loop coefficients: nloop rep loop_coeff  multiplicity\n");
    for(i=0;i<NREPS;i++) for(j=0;j<NLOOP;j++) {
        node0_printf("                    %d %d      %e     %d\n",
            j,i,loop_coeff[j][i],loop_num[j]);
    }
*/
}


#define __escape__(PTR) { asm volatile("" : : "g"(PTR) : "memory");  }
#define __escape_mem__() { asm volatile("" : : : "memory");  }

#define N 10000

int main() {


  for(int repeat = 0; repeat<N; repeat++) {
    make_loop_table();
    __escape__(loop_table);
    __escape__(loop_num);
  }

  return 0;
}

