
#include <cstdio>

#include "447.dealII/vector.h"
#include "447.dealII/vector.templates.h"

#include "common.h"

#define NRep 10000
int main() {
  int n = 262144;
  __escape__(&n);
  Vector<double> Vec1(n);
  Vector<float> Vec2(n);
  //Vec1 = 3.1415;
  Vec2 = 2.71828f;
  #pragma nounroll
  for(int repeat = 0; repeat<NRep; repeat++) {
    double Num = 1.4142;
    __escape__(&Num);
    __escape__(&Vec1);
    __escape__(&Vec2);
    Vec1.equ(Num, Vec2);
    __escape__(&Vec1);
    __escape__(&Vec2);
  }
  return 0;
}
