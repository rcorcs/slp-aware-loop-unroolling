
#include <cstdio>

#include "447.dealII/full_matrix.h"
#include "447.dealII/full_matrix.templates.h"

#include "common.h"

#define NRep 10000
int main() {
  int n = 516;
  __escape__(&n);
  FullMatrix<float> Mat(n);
  #pragma nounroll
  for(int repeat = 0; repeat<NRep; repeat++) {
    double Num = 2.71828;
    __escape__(&Num);
    __escape__(&Mat);
    Mat /= Num;
    __escape__(&Mat);
  }
  return 0;
}
