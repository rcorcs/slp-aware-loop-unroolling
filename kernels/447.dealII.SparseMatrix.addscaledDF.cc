
#include <cstdio>

#include "447.dealII/sparse_matrix.h"
#include "447.dealII/sparse_matrix.templates.h"

#include "common.h"

#define NRep 10000
int main() {
  int n = 2048;
  __escape__(&n);
  SparseMatrix<double> Mat1(SparsityPattern(n,n,n/2));
  SparseMatrix<float> Mat2(SparsityPattern(n,n,n/2));
  #pragma nounroll
  for(int repeat = 0; repeat<NRep; repeat++) {
    double Num = 2.71828;
    __escape__(&Num);
    __escape__(&Mat1);
    __escape__(&Mat2);
    Mat1.add_scaled(Num,Mat2);
    Mat1.add_scaled(Num,Mat2);
    Mat1.add_scaled(Num,Mat2);
    Mat1.add_scaled(Num,Mat2);
    __escape__(&Mat1);
    __escape__(&Mat2);
  }
  return 0;
}
