LLVM_PATH=/home/rodrigo/tmp/build/bin/

OPT="-O3 -march=native -mtune=native -ffast-math" # -fno-inline-functions"
CFLAG=-lm
SLPSETTINGS="-mllvm -slp-vectorize-hor-store=true -mllvm -slp-vectorize-hor=true"
LVSETTINGS=

EXT="${1##*.}"
if [ $EXT == "c" ]; then
 CC=${LLVM_PATH}clang
else
 CC=${LLVM_PATH}clang++
fi

${CC} $1 ${OPT} ${SLPSETTINGS} -o $(basename $1).std ${CFLAG}
${CC} $1 ${OPT} ${SLPSETTINGS} -o $(basename $1).std-valu -mllvm -slp-unroll-loops=true -mllvm -slp-unroll-runtime=true ${CFLAG}


${CC} $1 ${OPT} -fno-vectorize -fno-slp-vectorize -o $(basename $1).baseline ${CFLAG}
${CC} $1 ${OPT} -fno-vectorize -fslp-vectorize -funroll-loops -mllvm -slp-unroll-loops=false ${SLPSETTINGS} -o $(basename $1).unroll-slp ${CFLAG}
#for Count in 2 4 8; do
#${CC} $1 ${OPT} -fno-vectorize -fslp-vectorize -funroll-loops -fno-inline-functions -mllvm -slp-unroll-loops=false -mllvm -unroll-count=${Count} -mllvm -unroll-allow-remainder=true -mllvm -unroll-runtime=1 ${SLPSETTINGS} -o $(basename $1).unroll-${Count}-slp ${CFLAG}
#done
${CC} $1 ${OPT} -fno-vectorize -fslp-vectorize -fno-unroll-loops -mllvm -slp-unroll-loops=false ${SLPSETTINGS} -o $(basename $1).slp ${CFLAG}
${CC} $1 ${OPT} -fno-vectorize -fslp-vectorize -mllvm -slp-unroll-loops=true -mllvm -slp-unroll-runtime=true ${SLPSETTINGS} -o $(basename $1).luslp ${CFLAG}
${CC} $1 ${OPT} -fvectorize -fno-slp-vectorize -o $(basename $1).vec ${CFLAG}


#${CC} $1 ${OPT} -fvectorize -fslp-vectorize -fno-unroll-loops -fno-inline-functions -mllvm -slp-unroll-loops=false ${SLPSETTINGS} -o $(basename $1).lv-slp ${CFLAG}
#${CC} $1 ${OPT} -fvectorize -fslp-vectorize  -funroll-loops -fno-inline-functions -mllvm -slp-unroll-loops=false ${SLPSETTINGS} -o $(basename $1).lv-unroll-slp ${CFLAG}
#${CC} $1 ${OPT} -fvectorize -fslp-vectorize -fno-unroll-loops -fno-inline-functions -mllvm -slp-unroll-loops=true -mllvm -slp-unroll-runtime=true  ${SLPSETTINGS} -o $(basename $1).lv-luslp ${CFLAG}

