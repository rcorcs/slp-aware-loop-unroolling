#include "433.milc.macros.h"


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


/* Directions, and a macro to give the opposite direction */
/*  These must go from 0 to 7 because they will be used to index an
    array. */
/* Also define NDIRS = number of directions */
#define XUP 0
#define YUP 1
#define ZUP 2
#define TUP 3
#define TDOWN 4
#define ZDOWN 5
#define YDOWN 6
#define XDOWN 7

#define NODIR -1  /* not a direction */

#define OPP_DIR(dir)    (7-(dir))       /* Opposite direction */
#define NDIRS 8                         /* number of directions */


/* defines NREPS NLOOP MAX_LENGTH MAX_NUM */
#define NREPS 1
#define NLOOP 3
#define MAX_LENGTH 6
#define MAX_NUM 16



#ifdef LOOPEND
#undef FORALLSITES
#define FORALLSITES(i,s) \
{ register int loopend; loopend=sites_on_node; \
for( i=0,  s=lattice ; i<loopend; i++,s++ )
#define END_LOOP }
#else
#define END_LOOP        /* define it to be nothing */
#endif

#define GOES_FORWARDS(dir) (dir<=TUP)
#define GOES_BACKWARDS(dir) (dir>TUP)



//char gauge_action_description[128];
int  gauge_action_nloops=NLOOP;
int  gauge_action_nreps=NREPS;
int loop_length[NLOOP];	/* lengths of various kinds of loops */
int loop_num[NLOOP];	/* number of rotations/reflections  for each kind */

    /* table of directions, 1 for each kind of loop */
int loop_ind[NLOOP][MAX_LENGTH];
    /* table of directions, for each rotation and reflection of each kind of
	loop.  tabulated with "canonical" starting point and direction. */
int loop_table[NLOOP][MAX_NUM][MAX_LENGTH];
    /* table of coefficients in action, for various "representations" (actually,
	powers of the trace) */
double loop_coeff[NLOOP][NREPS];
    /* for each rotation/reflection, an integer distinct for each starting
	point, or each cyclic permutation of the links */
int loop_char[MAX_NUM];
    /* for each kind of loop for each rotation/reflection, the expectation
	value of the loop */
double loop_expect[NLOOP][NREPS][MAX_NUM];



double get_gauge_fix_action(int gauge_dir,int parity)
{
  /* Adds up the gauge fixing action for sites of given parity */
  /* Returns average over these sites */
  /* The average is normalized to a maximum of 1 when all */
  /* links are unit matrices */

  register int dir,i,ndir;
  register site *s;
  register su3_matrix *m1, *m2;
  double gauge_fix_action;
  complex trace;

  gauge_fix_action = 0.0;

  FORSOMEPARITY(i,s,parity)
    {
      FORALLUPDIRBUT(gauge_dir,dir)
        {
          m1 = &(s->link[dir]);
          m2 = (su3_matrix *)gen_pt[dir][i];

          trace = trace_su3(m1);
          gauge_fix_action += (double)trace.real;

          trace = trace_su3(m2);
          gauge_fix_action += (double)trace.real;
        }
    }

  /* Count number of terms to average */
  ndir = 0; FORALLUPDIRBUT(gauge_dir,dir)ndir++;

  /* Sum over all sites of this parity */
  g_doublesum( &gauge_fix_action);

  /* Average is normalized to max of 1/2 on sites of one parity */
  return(gauge_fix_action /((double)(6*ndir*nx*ny*nz*nt)));
} /* get_gauge_fix_action */



#define __escape__(PTR) { asm volatile("" : : "g"(PTR) : "memory");  }
#define __escape_mem__() { asm volatile("" : : : "memory");  }

#define N 10000

int main() {


  for(int repeat = 0; repeat<N; repeat++) {
    __escape__(loop_table);
    __escape__(loop_num);
  }

  return 0;
}


