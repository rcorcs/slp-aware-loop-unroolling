; ModuleID = '456.hmmer.DScale.c'
source_filename = "456.hmmer.DScale.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; Function Attrs: noinline norecurse nounwind uwtable writeonly
define dso_local void @DSet(double* nocapture %vec, i32 %n, double %value) local_unnamed_addr #0 {
entry:
  %cmp4 = icmp sgt i32 %n, 0
  br i1 %cmp4, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  %min.iters.check = icmp ult i32 %n, 4
  br i1 %min.iters.check, label %for.body.preheader8, label %vector.ph

vector.ph:                                        ; preds = %for.body.preheader
  %n.vec = and i64 %wide.trip.count, 4294967292
  %broadcast.splatinsert6 = insertelement <4 x double> undef, double %value, i32 0
  %broadcast.splat7 = shufflevector <4 x double> %broadcast.splatinsert6, <4 x double> undef, <4 x i32> zeroinitializer
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph
  %index = phi i64 [ 0, %vector.ph ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds double, double* %vec, i64 %index
  %1 = bitcast double* %0 to <4 x double>*
  store <4 x double> %broadcast.splat7, <4 x double>* %1, align 8, !tbaa !2
  %index.next = add i64 %index, 4
  %2 = icmp eq i64 %index.next, %n.vec
  br i1 %2, label %middle.block, label %vector.body, !llvm.loop !6

middle.block:                                     ; preds = %vector.body
  %cmp.n = icmp eq i64 %n.vec, %wide.trip.count
  br i1 %cmp.n, label %for.end, label %for.body.preheader8

for.body.preheader8:                              ; preds = %middle.block, %for.body.preheader
  %indvars.iv.ph = phi i64 [ 0, %for.body.preheader ], [ %n.vec, %middle.block ]
  br label %for.body

for.body:                                         ; preds = %for.body.preheader8, %for.body
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body ], [ %indvars.iv.ph, %for.body.preheader8 ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  store double %value, double* %arrayidx, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body, !llvm.loop !8

for.end:                                          ; preds = %for.body, %middle.block, %entry
  ret void
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start.p0i8(i64, i8* nocapture) #1

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end.p0i8(i64, i8* nocapture) #1

; Function Attrs: noinline norecurse nounwind uwtable writeonly
define dso_local void @FSet(float* nocapture %vec, i32 %n, float %value) local_unnamed_addr #0 {
entry:
  %cmp4 = icmp sgt i32 %n, 0
  br i1 %cmp4, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  %min.iters.check = icmp ult i32 %n, 8
  br i1 %min.iters.check, label %for.body.preheader8, label %vector.ph

vector.ph:                                        ; preds = %for.body.preheader
  %n.vec = and i64 %wide.trip.count, 4294967288
  %broadcast.splatinsert6 = insertelement <8 x float> undef, float %value, i32 0
  %broadcast.splat7 = shufflevector <8 x float> %broadcast.splatinsert6, <8 x float> undef, <8 x i32> zeroinitializer
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph
  %index = phi i64 [ 0, %vector.ph ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds float, float* %vec, i64 %index
  %1 = bitcast float* %0 to <8 x float>*
  store <8 x float> %broadcast.splat7, <8 x float>* %1, align 4, !tbaa !10
  %index.next = add i64 %index, 8
  %2 = icmp eq i64 %index.next, %n.vec
  br i1 %2, label %middle.block, label %vector.body, !llvm.loop !12

middle.block:                                     ; preds = %vector.body
  %cmp.n = icmp eq i64 %n.vec, %wide.trip.count
  br i1 %cmp.n, label %for.end, label %for.body.preheader8

for.body.preheader8:                              ; preds = %middle.block, %for.body.preheader
  %indvars.iv.ph = phi i64 [ 0, %for.body.preheader ], [ %n.vec, %middle.block ]
  br label %for.body

for.body:                                         ; preds = %for.body.preheader8, %for.body
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body ], [ %indvars.iv.ph, %for.body.preheader8 ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  store float %value, float* %arrayidx, align 4, !tbaa !10
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body, !llvm.loop !13

for.end:                                          ; preds = %for.body, %middle.block, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @DScale(double* nocapture %vec, i32 %n, double %scale) local_unnamed_addr #2 {
entry:
  %cmp4 = icmp sgt i32 %n, 0
  br i1 %cmp4, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  %min.iters.check = icmp ult i32 %n, 4
  br i1 %min.iters.check, label %for.body.preheader8, label %vector.ph

vector.ph:                                        ; preds = %for.body.preheader
  %n.vec = and i64 %wide.trip.count, 4294967292
  %broadcast.splatinsert6 = insertelement <4 x double> undef, double %scale, i32 0
  %broadcast.splat7 = shufflevector <4 x double> %broadcast.splatinsert6, <4 x double> undef, <4 x i32> zeroinitializer
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph
  %index = phi i64 [ 0, %vector.ph ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds double, double* %vec, i64 %index
  %1 = bitcast double* %0 to <4 x double>*
  %wide.load = load <4 x double>, <4 x double>* %1, align 8, !tbaa !2
  %2 = fmul <4 x double> %wide.load, %broadcast.splat7
  %3 = bitcast double* %0 to <4 x double>*
  store <4 x double> %2, <4 x double>* %3, align 8, !tbaa !2
  %index.next = add i64 %index, 4
  %4 = icmp eq i64 %index.next, %n.vec
  br i1 %4, label %middle.block, label %vector.body, !llvm.loop !14

middle.block:                                     ; preds = %vector.body
  %cmp.n = icmp eq i64 %n.vec, %wide.trip.count
  br i1 %cmp.n, label %for.end, label %for.body.preheader8

for.body.preheader8:                              ; preds = %middle.block, %for.body.preheader
  %indvars.iv.ph = phi i64 [ 0, %for.body.preheader ], [ %n.vec, %middle.block ]
  br label %for.body

for.body:                                         ; preds = %for.body.preheader8, %for.body
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body ], [ %indvars.iv.ph, %for.body.preheader8 ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %5 = load double, double* %arrayidx, align 8, !tbaa !2
  %mul = fmul double %5, %scale
  store double %mul, double* %arrayidx, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body, !llvm.loop !15

for.end:                                          ; preds = %for.body, %middle.block, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @FScale(float* nocapture %vec, i32 %n, float %scale) local_unnamed_addr #2 {
entry:
  %cmp4 = icmp sgt i32 %n, 0
  br i1 %cmp4, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  %min.iters.check = icmp ult i32 %n, 8
  br i1 %min.iters.check, label %for.body.preheader8, label %vector.ph

vector.ph:                                        ; preds = %for.body.preheader
  %n.vec = and i64 %wide.trip.count, 4294967288
  %broadcast.splatinsert6 = insertelement <8 x float> undef, float %scale, i32 0
  %broadcast.splat7 = shufflevector <8 x float> %broadcast.splatinsert6, <8 x float> undef, <8 x i32> zeroinitializer
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph
  %index = phi i64 [ 0, %vector.ph ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds float, float* %vec, i64 %index
  %1 = bitcast float* %0 to <8 x float>*
  %wide.load = load <8 x float>, <8 x float>* %1, align 4, !tbaa !10
  %2 = fmul <8 x float> %wide.load, %broadcast.splat7
  %3 = bitcast float* %0 to <8 x float>*
  store <8 x float> %2, <8 x float>* %3, align 4, !tbaa !10
  %index.next = add i64 %index, 8
  %4 = icmp eq i64 %index.next, %n.vec
  br i1 %4, label %middle.block, label %vector.body, !llvm.loop !16

middle.block:                                     ; preds = %vector.body
  %cmp.n = icmp eq i64 %n.vec, %wide.trip.count
  br i1 %cmp.n, label %for.end, label %for.body.preheader8

for.body.preheader8:                              ; preds = %middle.block, %for.body.preheader
  %indvars.iv.ph = phi i64 [ 0, %for.body.preheader ], [ %n.vec, %middle.block ]
  br label %for.body

for.body:                                         ; preds = %for.body.preheader8, %for.body
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body ], [ %indvars.iv.ph, %for.body.preheader8 ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %5 = load float, float* %arrayidx, align 4, !tbaa !10
  %mul = fmul float %5, %scale
  store float %mul, float* %arrayidx, align 4, !tbaa !10
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body, !llvm.loop !17

for.end:                                          ; preds = %for.body, %middle.block, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local double @DSum(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp6 = icmp sgt i32 %n, 0
  br i1 %cmp6, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %sum.07 = phi double [ 0.000000e+00, %for.body.preheader ], [ %add, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %add = fadd double %sum.07, %0
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %sum.0.lcssa = phi double [ 0.000000e+00, %entry ], [ %add, %for.body ]
  ret double %sum.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local float @FSum(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp6 = icmp sgt i32 %n, 0
  br i1 %cmp6, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %sum.07 = phi float [ 0.000000e+00, %for.body.preheader ], [ %add, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !10
  %add = fadd float %sum.07, %0
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %sum.0.lcssa = phi float [ 0.000000e+00, %entry ], [ %add, %for.body ]
  ret float %sum.0.lcssa
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @DAdd(double* nocapture %vec1, double* nocapture readonly %vec2, i32 %n) local_unnamed_addr #2 {
entry:
  %cmp7 = icmp sgt i32 %n, 0
  br i1 %cmp7, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  %min.iters.check = icmp ult i32 %n, 4
  br i1 %min.iters.check, label %for.body.preheader15, label %vector.memcheck

vector.memcheck:                                  ; preds = %for.body.preheader
  %scevgep = getelementptr double, double* %vec1, i64 %wide.trip.count
  %scevgep12 = getelementptr double, double* %vec2, i64 %wide.trip.count
  %bound0 = icmp ugt double* %scevgep12, %vec1
  %bound1 = icmp ugt double* %scevgep, %vec2
  %found.conflict = and i1 %bound0, %bound1
  br i1 %found.conflict, label %for.body.preheader15, label %vector.ph

vector.ph:                                        ; preds = %vector.memcheck
  %n.vec = and i64 %wide.trip.count, 4294967292
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph
  %index = phi i64 [ 0, %vector.ph ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds double, double* %vec2, i64 %index
  %1 = bitcast double* %0 to <4 x double>*
  %wide.load = load <4 x double>, <4 x double>* %1, align 8, !tbaa !2, !alias.scope !18
  %2 = getelementptr inbounds double, double* %vec1, i64 %index
  %3 = bitcast double* %2 to <4 x double>*
  %wide.load14 = load <4 x double>, <4 x double>* %3, align 8, !tbaa !2, !alias.scope !21, !noalias !18
  %4 = fadd <4 x double> %wide.load, %wide.load14
  %5 = bitcast double* %2 to <4 x double>*
  store <4 x double> %4, <4 x double>* %5, align 8, !tbaa !2, !alias.scope !21, !noalias !18
  %index.next = add i64 %index, 4
  %6 = icmp eq i64 %index.next, %n.vec
  br i1 %6, label %middle.block, label %vector.body, !llvm.loop !23

middle.block:                                     ; preds = %vector.body
  %cmp.n = icmp eq i64 %n.vec, %wide.trip.count
  br i1 %cmp.n, label %for.end, label %for.body.preheader15

for.body.preheader15:                             ; preds = %middle.block, %vector.memcheck, %for.body.preheader
  %indvars.iv.ph = phi i64 [ 0, %vector.memcheck ], [ 0, %for.body.preheader ], [ %n.vec, %middle.block ]
  br label %for.body

for.body:                                         ; preds = %for.body.preheader15, %for.body
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body ], [ %indvars.iv.ph, %for.body.preheader15 ]
  %arrayidx = getelementptr inbounds double, double* %vec2, i64 %indvars.iv
  %7 = load double, double* %arrayidx, align 8, !tbaa !2
  %arrayidx2 = getelementptr inbounds double, double* %vec1, i64 %indvars.iv
  %8 = load double, double* %arrayidx2, align 8, !tbaa !2
  %add = fadd double %7, %8
  store double %add, double* %arrayidx2, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body, !llvm.loop !24

for.end:                                          ; preds = %for.body, %middle.block, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @FAdd(float* nocapture %vec1, float* nocapture readonly %vec2, i32 %n) local_unnamed_addr #2 {
entry:
  %cmp7 = icmp sgt i32 %n, 0
  br i1 %cmp7, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  %min.iters.check = icmp ult i32 %n, 8
  br i1 %min.iters.check, label %for.body.preheader15, label %vector.memcheck

vector.memcheck:                                  ; preds = %for.body.preheader
  %scevgep = getelementptr float, float* %vec1, i64 %wide.trip.count
  %scevgep12 = getelementptr float, float* %vec2, i64 %wide.trip.count
  %bound0 = icmp ugt float* %scevgep12, %vec1
  %bound1 = icmp ugt float* %scevgep, %vec2
  %found.conflict = and i1 %bound0, %bound1
  br i1 %found.conflict, label %for.body.preheader15, label %vector.ph

vector.ph:                                        ; preds = %vector.memcheck
  %n.vec = and i64 %wide.trip.count, 4294967288
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph
  %index = phi i64 [ 0, %vector.ph ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds float, float* %vec2, i64 %index
  %1 = bitcast float* %0 to <8 x float>*
  %wide.load = load <8 x float>, <8 x float>* %1, align 4, !tbaa !10, !alias.scope !25
  %2 = getelementptr inbounds float, float* %vec1, i64 %index
  %3 = bitcast float* %2 to <8 x float>*
  %wide.load14 = load <8 x float>, <8 x float>* %3, align 4, !tbaa !10, !alias.scope !28, !noalias !25
  %4 = fadd <8 x float> %wide.load, %wide.load14
  %5 = bitcast float* %2 to <8 x float>*
  store <8 x float> %4, <8 x float>* %5, align 4, !tbaa !10, !alias.scope !28, !noalias !25
  %index.next = add i64 %index, 8
  %6 = icmp eq i64 %index.next, %n.vec
  br i1 %6, label %middle.block, label %vector.body, !llvm.loop !30

middle.block:                                     ; preds = %vector.body
  %cmp.n = icmp eq i64 %n.vec, %wide.trip.count
  br i1 %cmp.n, label %for.end, label %for.body.preheader15

for.body.preheader15:                             ; preds = %middle.block, %vector.memcheck, %for.body.preheader
  %indvars.iv.ph = phi i64 [ 0, %vector.memcheck ], [ 0, %for.body.preheader ], [ %n.vec, %middle.block ]
  br label %for.body

for.body:                                         ; preds = %for.body.preheader15, %for.body
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body ], [ %indvars.iv.ph, %for.body.preheader15 ]
  %arrayidx = getelementptr inbounds float, float* %vec2, i64 %indvars.iv
  %7 = load float, float* %arrayidx, align 4, !tbaa !10
  %arrayidx2 = getelementptr inbounds float, float* %vec1, i64 %indvars.iv
  %8 = load float, float* %arrayidx2, align 4, !tbaa !10
  %add = fadd float %7, %8
  store float %add, float* %arrayidx2, align 4, !tbaa !10
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body, !llvm.loop !31

for.end:                                          ; preds = %for.body, %middle.block, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @DCopy(double* nocapture %vec1, double* nocapture readonly %vec2, i32 %n) local_unnamed_addr #2 {
entry:
  %cmp7 = icmp sgt i32 %n, 0
  br i1 %cmp7, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  %min.iters.check = icmp ult i32 %n, 4
  br i1 %min.iters.check, label %for.body.preheader15, label %vector.memcheck

vector.memcheck:                                  ; preds = %for.body.preheader
  %scevgep = getelementptr double, double* %vec1, i64 %wide.trip.count
  %scevgep13 = getelementptr double, double* %vec2, i64 %wide.trip.count
  %bound0 = icmp ugt double* %scevgep13, %vec1
  %bound1 = icmp ugt double* %scevgep, %vec2
  %found.conflict = and i1 %bound0, %bound1
  br i1 %found.conflict, label %for.body.preheader15, label %vector.ph

vector.ph:                                        ; preds = %vector.memcheck
  %n.vec = and i64 %wide.trip.count, 4294967292
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph
  %index = phi i64 [ 0, %vector.ph ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds double, double* %vec2, i64 %index
  %1 = bitcast double* %0 to <4 x i64>*
  %wide.load = load <4 x i64>, <4 x i64>* %1, align 8, !tbaa !2, !alias.scope !32
  %2 = getelementptr inbounds double, double* %vec1, i64 %index
  %3 = bitcast double* %2 to <4 x i64>*
  store <4 x i64> %wide.load, <4 x i64>* %3, align 8, !tbaa !2, !alias.scope !35, !noalias !32
  %index.next = add i64 %index, 4
  %4 = icmp eq i64 %index.next, %n.vec
  br i1 %4, label %middle.block, label %vector.body, !llvm.loop !37

middle.block:                                     ; preds = %vector.body
  %cmp.n = icmp eq i64 %n.vec, %wide.trip.count
  br i1 %cmp.n, label %for.end, label %for.body.preheader15

for.body.preheader15:                             ; preds = %middle.block, %vector.memcheck, %for.body.preheader
  %indvars.iv.ph = phi i64 [ 0, %vector.memcheck ], [ 0, %for.body.preheader ], [ %n.vec, %middle.block ]
  br label %for.body

for.body:                                         ; preds = %for.body.preheader15, %for.body
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body ], [ %indvars.iv.ph, %for.body.preheader15 ]
  %arrayidx = getelementptr inbounds double, double* %vec2, i64 %indvars.iv
  %5 = bitcast double* %arrayidx to i64*
  %6 = load i64, i64* %5, align 8, !tbaa !2
  %arrayidx2 = getelementptr inbounds double, double* %vec1, i64 %indvars.iv
  %7 = bitcast double* %arrayidx2 to i64*
  store i64 %6, i64* %7, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body, !llvm.loop !38

for.end:                                          ; preds = %for.body, %middle.block, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @FCopy(float* nocapture %vec1, float* nocapture readonly %vec2, i32 %n) local_unnamed_addr #2 {
entry:
  %cmp7 = icmp sgt i32 %n, 0
  br i1 %cmp7, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  %min.iters.check = icmp ult i32 %n, 8
  br i1 %min.iters.check, label %for.body.preheader15, label %vector.memcheck

vector.memcheck:                                  ; preds = %for.body.preheader
  %scevgep = getelementptr float, float* %vec1, i64 %wide.trip.count
  %scevgep13 = getelementptr float, float* %vec2, i64 %wide.trip.count
  %bound0 = icmp ugt float* %scevgep13, %vec1
  %bound1 = icmp ugt float* %scevgep, %vec2
  %found.conflict = and i1 %bound0, %bound1
  br i1 %found.conflict, label %for.body.preheader15, label %vector.ph

vector.ph:                                        ; preds = %vector.memcheck
  %n.vec = and i64 %wide.trip.count, 4294967288
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph
  %index = phi i64 [ 0, %vector.ph ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds float, float* %vec2, i64 %index
  %1 = bitcast float* %0 to <8 x i32>*
  %wide.load = load <8 x i32>, <8 x i32>* %1, align 4, !tbaa !10, !alias.scope !39
  %2 = getelementptr inbounds float, float* %vec1, i64 %index
  %3 = bitcast float* %2 to <8 x i32>*
  store <8 x i32> %wide.load, <8 x i32>* %3, align 4, !tbaa !10, !alias.scope !42, !noalias !39
  %index.next = add i64 %index, 8
  %4 = icmp eq i64 %index.next, %n.vec
  br i1 %4, label %middle.block, label %vector.body, !llvm.loop !44

middle.block:                                     ; preds = %vector.body
  %cmp.n = icmp eq i64 %n.vec, %wide.trip.count
  br i1 %cmp.n, label %for.end, label %for.body.preheader15

for.body.preheader15:                             ; preds = %middle.block, %vector.memcheck, %for.body.preheader
  %indvars.iv.ph = phi i64 [ 0, %vector.memcheck ], [ 0, %for.body.preheader ], [ %n.vec, %middle.block ]
  br label %for.body

for.body:                                         ; preds = %for.body.preheader15, %for.body
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body ], [ %indvars.iv.ph, %for.body.preheader15 ]
  %arrayidx = getelementptr inbounds float, float* %vec2, i64 %indvars.iv
  %5 = bitcast float* %arrayidx to i32*
  %6 = load i32, i32* %5, align 4, !tbaa !10
  %arrayidx2 = getelementptr inbounds float, float* %vec1, i64 %indvars.iv
  %7 = bitcast float* %arrayidx2 to i32*
  store i32 %6, i32* %7, align 4, !tbaa !10
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body, !llvm.loop !45

for.end:                                          ; preds = %for.body, %middle.block, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local double @DDot(double* nocapture readonly %vec1, double* nocapture readonly %vec2, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp9 = icmp sgt i32 %n, 0
  br i1 %cmp9, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %result.010 = phi double [ 0.000000e+00, %for.body.preheader ], [ %add, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec1, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %arrayidx2 = getelementptr inbounds double, double* %vec2, i64 %indvars.iv
  %1 = load double, double* %arrayidx2, align 8, !tbaa !2
  %mul = fmul double %0, %1
  %add = fadd double %result.010, %mul
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %result.0.lcssa = phi double [ 0.000000e+00, %entry ], [ %add, %for.body ]
  ret double %result.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local float @FDot(float* nocapture readonly %vec1, float* nocapture readonly %vec2, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp9 = icmp sgt i32 %n, 0
  br i1 %cmp9, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %result.010 = phi float [ 0.000000e+00, %for.body.preheader ], [ %add, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec1, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !10
  %arrayidx2 = getelementptr inbounds float, float* %vec2, i64 %indvars.iv
  %1 = load float, float* %arrayidx2, align 4, !tbaa !10
  %mul = fmul float %0, %1
  %add = fadd float %result.010, %mul
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %result.0.lcssa = phi float [ 0.000000e+00, %entry ], [ %add, %for.body ]
  ret float %result.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local double @DMax(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %0 = load double, double* %vec, align 8, !tbaa !2
  %cmp13 = icmp sgt i32 %n, 1
  br i1 %cmp13, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.015 = phi double [ %0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx1 = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %1 = load double, double* %arrayidx1, align 8, !tbaa !2
  %cmp2 = fcmp ogt double %1, %best.015
  %best.1 = select i1 %cmp2, double %1, double %best.015
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi double [ %0, %entry ], [ %best.1, %for.body ]
  ret double %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local float @FMax(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %0 = load float, float* %vec, align 4, !tbaa !10
  %cmp13 = icmp sgt i32 %n, 1
  br i1 %cmp13, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.015 = phi float [ %0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx1 = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %1 = load float, float* %arrayidx1, align 4, !tbaa !10
  %cmp2 = fcmp ogt float %1, %best.015
  %best.1 = select i1 %cmp2, float %1, float %best.015
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi float [ %0, %entry ], [ %best.1, %for.body ]
  ret float %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local double @DMin(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %0 = load double, double* %vec, align 8, !tbaa !2
  %cmp13 = icmp sgt i32 %n, 1
  br i1 %cmp13, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.015 = phi double [ %0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx1 = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %1 = load double, double* %arrayidx1, align 8, !tbaa !2
  %cmp2 = fcmp olt double %1, %best.015
  %best.1 = select i1 %cmp2, double %1, double %best.015
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi double [ %0, %entry ], [ %best.1, %for.body ]
  ret double %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local float @FMin(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %0 = load float, float* %vec, align 4, !tbaa !10
  %cmp13 = icmp sgt i32 %n, 1
  br i1 %cmp13, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.015 = phi float [ %0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx1 = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %1 = load float, float* %arrayidx1, align 4, !tbaa !10
  %cmp2 = fcmp olt float %1, %best.015
  %best.1 = select i1 %cmp2, float %1, float %best.015
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi float [ %0, %entry ], [ %best.1, %for.body ]
  ret float %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local i32 @DArgMax(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp11 = icmp sgt i32 %n, 1
  br i1 %cmp11, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.013 = phi i32 [ 0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %idxprom1 = sext i32 %best.013 to i64
  %arrayidx2 = getelementptr inbounds double, double* %vec, i64 %idxprom1
  %1 = load double, double* %arrayidx2, align 8, !tbaa !2
  %cmp3 = fcmp ogt double %0, %1
  %2 = trunc i64 %indvars.iv to i32
  %best.1 = select i1 %cmp3, i32 %2, i32 %best.013
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi i32 [ 0, %entry ], [ %best.1, %for.body ]
  ret i32 %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local i32 @FArgMax(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp11 = icmp sgt i32 %n, 1
  br i1 %cmp11, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.013 = phi i32 [ 0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !10
  %idxprom1 = sext i32 %best.013 to i64
  %arrayidx2 = getelementptr inbounds float, float* %vec, i64 %idxprom1
  %1 = load float, float* %arrayidx2, align 4, !tbaa !10
  %cmp3 = fcmp ogt float %0, %1
  %2 = trunc i64 %indvars.iv to i32
  %best.1 = select i1 %cmp3, i32 %2, i32 %best.013
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi i32 [ 0, %entry ], [ %best.1, %for.body ]
  ret i32 %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local i32 @DArgMin(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp11 = icmp sgt i32 %n, 1
  br i1 %cmp11, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.013 = phi i32 [ 0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %idxprom1 = sext i32 %best.013 to i64
  %arrayidx2 = getelementptr inbounds double, double* %vec, i64 %idxprom1
  %1 = load double, double* %arrayidx2, align 8, !tbaa !2
  %cmp3 = fcmp olt double %0, %1
  %2 = trunc i64 %indvars.iv to i32
  %best.1 = select i1 %cmp3, i32 %2, i32 %best.013
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi i32 [ 0, %entry ], [ %best.1, %for.body ]
  ret i32 %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local i32 @FArgMin(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp11 = icmp sgt i32 %n, 1
  br i1 %cmp11, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.013 = phi i32 [ 0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !10
  %idxprom1 = sext i32 %best.013 to i64
  %arrayidx2 = getelementptr inbounds float, float* %vec, i64 %idxprom1
  %1 = load float, float* %arrayidx2, align 4, !tbaa !10
  %cmp3 = fcmp olt float %0, %1
  %2 = trunc i64 %indvars.iv to i32
  %best.1 = select i1 %cmp3, i32 %2, i32 %best.013
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi i32 [ 0, %entry ], [ %best.1, %for.body ]
  ret i32 %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @DNorm(double* nocapture %vec, i32 %n) local_unnamed_addr #2 {
entry:
  %call = tail call double @DSum(double* %vec, i32 %n)
  %cmp = fcmp une double %call, 0.000000e+00
  %cmp125 = icmp sgt i32 %n, 0
  br i1 %cmp, label %for.cond.preheader, label %for.cond2.preheader

for.cond2.preheader:                              ; preds = %entry
  br i1 %cmp125, label %for.body4.lr.ph, label %if.end

for.body4.lr.ph:                                  ; preds = %for.cond2.preheader
  %conv = sitofp i32 %n to double
  %div5 = fdiv double 1.000000e+00, %conv
  %wide.trip.count32 = zext i32 %n to i64
  %min.iters.check = icmp ult i32 %n, 4
  br i1 %min.iters.check, label %for.body4.preheader, label %vector.ph

for.body4.preheader:                              ; preds = %middle.block, %for.body4.lr.ph
  %indvars.iv30.ph = phi i64 [ 0, %for.body4.lr.ph ], [ %n.vec, %middle.block ]
  br label %for.body4

vector.ph:                                        ; preds = %for.body4.lr.ph
  %n.vec = and i64 %wide.trip.count32, 4294967292
  %broadcast.splatinsert36 = insertelement <4 x double> undef, double %div5, i32 0
  %broadcast.splat37 = shufflevector <4 x double> %broadcast.splatinsert36, <4 x double> undef, <4 x i32> zeroinitializer
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph
  %index = phi i64 [ 0, %vector.ph ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds double, double* %vec, i64 %index
  %1 = bitcast double* %0 to <4 x double>*
  store <4 x double> %broadcast.splat37, <4 x double>* %1, align 8, !tbaa !2
  %index.next = add i64 %index, 4
  %2 = icmp eq i64 %index.next, %n.vec
  br i1 %2, label %middle.block, label %vector.body, !llvm.loop !46

middle.block:                                     ; preds = %vector.body
  %cmp.n = icmp eq i64 %n.vec, %wide.trip.count32
  br i1 %cmp.n, label %if.end, label %for.body4.preheader

for.cond.preheader:                               ; preds = %entry
  br i1 %cmp125, label %for.body.preheader, label %if.end

for.body.preheader:                               ; preds = %for.cond.preheader
  %wide.trip.count = zext i32 %n to i64
  %min.iters.check41 = icmp ult i32 %n, 4
  br i1 %min.iters.check41, label %for.body.preheader54, label %vector.ph42

vector.ph42:                                      ; preds = %for.body.preheader
  %n.vec44 = and i64 %wide.trip.count, 4294967292
  %broadcast.splatinsert52 = insertelement <4 x double> undef, double %call, i32 0
  %broadcast.splat53 = shufflevector <4 x double> %broadcast.splatinsert52, <4 x double> undef, <4 x i32> zeroinitializer
  br label %vector.body38

vector.body38:                                    ; preds = %vector.body38, %vector.ph42
  %index45 = phi i64 [ 0, %vector.ph42 ], [ %index.next46, %vector.body38 ]
  %3 = getelementptr inbounds double, double* %vec, i64 %index45
  %4 = bitcast double* %3 to <4 x double>*
  %wide.load = load <4 x double>, <4 x double>* %4, align 8, !tbaa !2
  %5 = fdiv <4 x double> %wide.load, %broadcast.splat53
  %6 = bitcast double* %3 to <4 x double>*
  store <4 x double> %5, <4 x double>* %6, align 8, !tbaa !2
  %index.next46 = add i64 %index45, 4
  %7 = icmp eq i64 %index.next46, %n.vec44
  br i1 %7, label %middle.block39, label %vector.body38, !llvm.loop !47

middle.block39:                                   ; preds = %vector.body38
  %cmp.n48 = icmp eq i64 %n.vec44, %wide.trip.count
  br i1 %cmp.n48, label %if.end, label %for.body.preheader54

for.body.preheader54:                             ; preds = %middle.block39, %for.body.preheader
  %indvars.iv.ph = phi i64 [ 0, %for.body.preheader ], [ %n.vec44, %middle.block39 ]
  br label %for.body

for.body:                                         ; preds = %for.body.preheader54, %for.body
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body ], [ %indvars.iv.ph, %for.body.preheader54 ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %8 = load double, double* %arrayidx, align 8, !tbaa !2
  %div = fdiv double %8, %call
  store double %div, double* %arrayidx, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %if.end, label %for.body, !llvm.loop !48

for.body4:                                        ; preds = %for.body4.preheader, %for.body4
  %indvars.iv30 = phi i64 [ %indvars.iv.next31, %for.body4 ], [ %indvars.iv30.ph, %for.body4.preheader ]
  %arrayidx7 = getelementptr inbounds double, double* %vec, i64 %indvars.iv30
  store double %div5, double* %arrayidx7, align 8, !tbaa !2
  %indvars.iv.next31 = add nuw nsw i64 %indvars.iv30, 1
  %exitcond33 = icmp eq i64 %indvars.iv.next31, %wide.trip.count32
  br i1 %exitcond33, label %if.end, label %for.body4, !llvm.loop !49

if.end:                                           ; preds = %for.body4, %for.body, %middle.block, %middle.block39, %for.cond2.preheader, %for.cond.preheader
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @FNorm(float* nocapture %vec, i32 %n) local_unnamed_addr #2 {
entry:
  %call = tail call float @FSum(float* %vec, i32 %n)
  %cmp = fcmp une float %call, 0.000000e+00
  %cmp231 = icmp sgt i32 %n, 0
  br i1 %cmp, label %for.cond.preheader, label %for.cond4.preheader

for.cond4.preheader:                              ; preds = %entry
  br i1 %cmp231, label %for.body7.lr.ph, label %if.end

for.body7.lr.ph:                                  ; preds = %for.cond4.preheader
  %conv8 = sitofp i32 %n to float
  %conv11 = fdiv float 1.000000e+00, %conv8
  %wide.trip.count38 = zext i32 %n to i64
  %min.iters.check = icmp ult i32 %n, 8
  br i1 %min.iters.check, label %for.body7.preheader, label %vector.ph

for.body7.preheader:                              ; preds = %middle.block, %for.body7.lr.ph
  %indvars.iv36.ph = phi i64 [ 0, %for.body7.lr.ph ], [ %n.vec, %middle.block ]
  br label %for.body7

vector.ph:                                        ; preds = %for.body7.lr.ph
  %n.vec = and i64 %wide.trip.count38, 4294967288
  %broadcast.splatinsert42 = insertelement <8 x float> undef, float %conv11, i32 0
  %broadcast.splat43 = shufflevector <8 x float> %broadcast.splatinsert42, <8 x float> undef, <8 x i32> zeroinitializer
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph
  %index = phi i64 [ 0, %vector.ph ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds float, float* %vec, i64 %index
  %1 = bitcast float* %0 to <8 x float>*
  store <8 x float> %broadcast.splat43, <8 x float>* %1, align 4, !tbaa !10
  %index.next = add i64 %index, 8
  %2 = icmp eq i64 %index.next, %n.vec
  br i1 %2, label %middle.block, label %vector.body, !llvm.loop !50

middle.block:                                     ; preds = %vector.body
  %cmp.n = icmp eq i64 %n.vec, %wide.trip.count38
  br i1 %cmp.n, label %if.end, label %for.body7.preheader

for.cond.preheader:                               ; preds = %entry
  br i1 %cmp231, label %for.body.preheader, label %if.end

for.body.preheader:                               ; preds = %for.cond.preheader
  %wide.trip.count = zext i32 %n to i64
  %min.iters.check47 = icmp ult i32 %n, 8
  br i1 %min.iters.check47, label %for.body.preheader60, label %vector.ph48

vector.ph48:                                      ; preds = %for.body.preheader
  %n.vec50 = and i64 %wide.trip.count, 4294967288
  %broadcast.splatinsert58 = insertelement <8 x float> undef, float %call, i32 0
  %broadcast.splat59 = shufflevector <8 x float> %broadcast.splatinsert58, <8 x float> undef, <8 x i32> zeroinitializer
  br label %vector.body44

vector.body44:                                    ; preds = %vector.body44, %vector.ph48
  %index51 = phi i64 [ 0, %vector.ph48 ], [ %index.next52, %vector.body44 ]
  %3 = getelementptr inbounds float, float* %vec, i64 %index51
  %4 = bitcast float* %3 to <8 x float>*
  %wide.load = load <8 x float>, <8 x float>* %4, align 4, !tbaa !10
  %5 = fdiv <8 x float> %wide.load, %broadcast.splat59
  %6 = bitcast float* %3 to <8 x float>*
  store <8 x float> %5, <8 x float>* %6, align 4, !tbaa !10
  %index.next52 = add i64 %index51, 8
  %7 = icmp eq i64 %index.next52, %n.vec50
  br i1 %7, label %middle.block45, label %vector.body44, !llvm.loop !51

middle.block45:                                   ; preds = %vector.body44
  %cmp.n54 = icmp eq i64 %n.vec50, %wide.trip.count
  br i1 %cmp.n54, label %if.end, label %for.body.preheader60

for.body.preheader60:                             ; preds = %middle.block45, %for.body.preheader
  %indvars.iv.ph = phi i64 [ 0, %for.body.preheader ], [ %n.vec50, %middle.block45 ]
  br label %for.body

for.body:                                         ; preds = %for.body.preheader60, %for.body
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body ], [ %indvars.iv.ph, %for.body.preheader60 ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %8 = load float, float* %arrayidx, align 4, !tbaa !10
  %div = fdiv float %8, %call
  store float %div, float* %arrayidx, align 4, !tbaa !10
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %if.end, label %for.body, !llvm.loop !52

for.body7:                                        ; preds = %for.body7.preheader, %for.body7
  %indvars.iv36 = phi i64 [ %indvars.iv.next37, %for.body7 ], [ %indvars.iv36.ph, %for.body7.preheader ]
  %arrayidx13 = getelementptr inbounds float, float* %vec, i64 %indvars.iv36
  store float %conv11, float* %arrayidx13, align 4, !tbaa !10
  %indvars.iv.next37 = add nuw nsw i64 %indvars.iv36, 1
  %exitcond39 = icmp eq i64 %indvars.iv.next37, %wide.trip.count38
  br i1 %exitcond39, label %if.end, label %for.body7, !llvm.loop !53

if.end:                                           ; preds = %for.body7, %for.body, %middle.block, %middle.block45, %for.cond4.preheader, %for.cond.preheader
  ret void
}

; Function Attrs: noinline nounwind uwtable
define dso_local void @DLog(double* nocapture %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %cmp15 = icmp sgt i32 %n, 0
  br i1 %cmp15, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.inc, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.inc ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %cmp1 = fcmp ogt double %0, 0.000000e+00
  br i1 %cmp1, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %call = tail call double @log(double %0) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then
  %storemerge = phi double [ %call, %if.then ], [ 0xFFEFFFFFFFFFFFFF, %for.body ]
  store double %storemerge, double* %arrayidx, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.inc, %entry
  ret void
}

; Function Attrs: nounwind
declare dso_local double @log(double) local_unnamed_addr #5

; Function Attrs: noinline nounwind uwtable
define dso_local void @FLog(float* nocapture %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %cmp18 = icmp sgt i32 %n, 0
  br i1 %cmp18, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.inc, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.inc ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !10
  %cmp1 = fcmp ogt float %0, 0.000000e+00
  br i1 %cmp1, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %conv = fpext float %0 to double
  %call = tail call double @log(double %conv) #6
  %conv6 = fptrunc double %call to float
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then
  %storemerge = phi float [ %conv6, %if.then ], [ 0xC7EFFFFFE0000000, %for.body ]
  store float %storemerge, float* %arrayidx, align 4, !tbaa !10
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.inc, %entry
  ret void
}

; Function Attrs: noinline nounwind uwtable
define dso_local void @DExp(double* nocapture %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %cmp8 = icmp sgt i32 %n, 0
  br i1 %cmp8, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %call = tail call double @exp(double %0) #6
  store double %call, double* %arrayidx, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: nounwind
declare dso_local double @exp(double) local_unnamed_addr #5

; Function Attrs: noinline nounwind uwtable
define dso_local void @FExp(float* nocapture %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %cmp9 = icmp sgt i32 %n, 0
  br i1 %cmp9, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !10
  %conv = fpext float %0 to double
  %call = tail call double @exp(double %conv) #6
  %conv1 = fptrunc double %call to float
  store float %conv1, float* %arrayidx, align 4, !tbaa !10
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: noinline nounwind uwtable
define dso_local double @DLogSum(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %call = tail call double @DMax(double* %vec, i32 %n)
  %cmp21 = icmp sgt i32 %n, 0
  br i1 %cmp21, label %for.body.lr.ph, label %for.end

for.body.lr.ph:                                   ; preds = %entry
  %sub = fadd double %call, -5.000000e+01
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.inc, %for.body.lr.ph
  %indvars.iv = phi i64 [ 0, %for.body.lr.ph ], [ %indvars.iv.next, %for.inc ]
  %sum.023 = phi double [ 0.000000e+00, %for.body.lr.ph ], [ %sum.1, %for.inc ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %cmp1 = fcmp ogt double %0, %sub
  br i1 %cmp1, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %sub4 = fsub double %0, %call
  %call5 = tail call double @exp(double %sub4) #6
  %add = fadd double %sum.023, %call5
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then
  %sum.1 = phi double [ %add, %if.then ], [ %sum.023, %for.body ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.inc, %entry
  %sum.0.lcssa = phi double [ 0.000000e+00, %entry ], [ %sum.1, %for.inc ]
  %call6 = tail call double @log(double %sum.0.lcssa) #6
  %add7 = fadd double %call, %call6
  ret double %add7
}

; Function Attrs: noinline nounwind uwtable
define dso_local float @FLogSum(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %call = tail call float @FMax(float* %vec, i32 %n)
  %cmp29 = icmp sgt i32 %n, 0
  %conv1 = fpext float %call to double
  br i1 %cmp29, label %for.body.lr.ph, label %for.end

for.body.lr.ph:                                   ; preds = %entry
  %sub = fadd double %conv1, -5.000000e+01
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.inc, %for.body.lr.ph
  %indvars.iv = phi i64 [ 0, %for.body.lr.ph ], [ %indvars.iv.next, %for.inc ]
  %sum.031 = phi float [ 0.000000e+00, %for.body.lr.ph ], [ %sum.1, %for.inc ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !10
  %conv = fpext float %0 to double
  %cmp2 = fcmp olt double %sub, %conv
  br i1 %cmp2, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %sub6 = fsub float %0, %call
  %conv7 = fpext float %sub6 to double
  %call8 = tail call double @exp(double %conv7) #6
  %conv9 = fpext float %sum.031 to double
  %add = fadd double %call8, %conv9
  %conv10 = fptrunc double %add to float
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then
  %sum.1 = phi float [ %conv10, %if.then ], [ %sum.031, %for.body ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.inc, %entry
  %sum.0.lcssa = phi float [ 0.000000e+00, %entry ], [ %sum.1, %for.inc ]
  %conv11 = fpext float %sum.0.lcssa to double
  %call12 = tail call double @log(double %conv11) #6
  %add14 = fadd double %call12, %conv1
  %conv15 = fptrunc double %add14 to float
  ret float %conv15
}

; Function Attrs: noinline nounwind uwtable
define dso_local i32 @main() local_unnamed_addr #4 {
entry:
  %Ans = alloca double, align 8
  %vla3 = alloca [2048 x double], align 16
  %vla3.sub = getelementptr inbounds [2048 x double], [2048 x double]* %vla3, i64 0, i64 0
  call void asm sideeffect "", "imr,~{memory},~{dirflag},~{fpsr},~{flags}"(double* nonnull %vla3.sub) #6, !srcloc !54
  %0 = bitcast double* %Ans to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* nonnull %0) #6
  %call = call double @DSum(double* nonnull %vla3.sub, i32 2048)
  store double %call, double* %Ans, align 8, !tbaa !2
  call void asm sideeffect "", "imr,~{memory},~{dirflag},~{fpsr},~{flags}"(double* nonnull %Ans) #6, !srcloc !55
  call void @llvm.lifetime.end.p0i8(i64 8, i8* nonnull %0) #6
  ret i32 0
}

attributes #0 = { noinline norecurse nounwind uwtable writeonly "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="skylake" "target-features"="+adx,+aes,+avx,+avx2,+bmi,+bmi2,+clflushopt,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+invpcid,+lzcnt,+mmx,+movbe,+mpx,+pclmul,+popcnt,+prfchw,+rdrnd,+rdseed,+rtm,+sahf,+sgx,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsavec,+xsaveopt,+xsaves" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind }
attributes #2 = { noinline norecurse nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="skylake" "target-features"="+adx,+aes,+avx,+avx2,+bmi,+bmi2,+clflushopt,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+invpcid,+lzcnt,+mmx,+movbe,+mpx,+pclmul,+popcnt,+prfchw,+rdrnd,+rdseed,+rtm,+sahf,+sgx,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsavec,+xsaveopt,+xsaves" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noinline norecurse nounwind readonly uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="skylake" "target-features"="+adx,+aes,+avx,+avx2,+bmi,+bmi2,+clflushopt,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+invpcid,+lzcnt,+mmx,+movbe,+mpx,+pclmul,+popcnt,+prfchw,+rdrnd,+rdseed,+rtm,+sahf,+sgx,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsavec,+xsaveopt,+xsaves" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="skylake" "target-features"="+adx,+aes,+avx,+avx2,+bmi,+bmi2,+clflushopt,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+invpcid,+lzcnt,+mmx,+movbe,+mpx,+pclmul,+popcnt,+prfchw,+rdrnd,+rdseed,+rtm,+sahf,+sgx,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsavec,+xsaveopt,+xsaves" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="skylake" "target-features"="+adx,+aes,+avx,+avx2,+bmi,+bmi2,+clflushopt,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+invpcid,+lzcnt,+mmx,+movbe,+mpx,+pclmul,+popcnt,+prfchw,+rdrnd,+rdseed,+rtm,+sahf,+sgx,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsavec,+xsaveopt,+xsaves" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 8.0.0 (trunk)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"double", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = distinct !{!6, !7}
!7 = !{!"llvm.loop.isvectorized", i32 1}
!8 = distinct !{!8, !9, !7}
!9 = !{!"llvm.loop.unroll.runtime.disable"}
!10 = !{!11, !11, i64 0}
!11 = !{!"float", !4, i64 0}
!12 = distinct !{!12, !7}
!13 = distinct !{!13, !9, !7}
!14 = distinct !{!14, !7}
!15 = distinct !{!15, !9, !7}
!16 = distinct !{!16, !7}
!17 = distinct !{!17, !9, !7}
!18 = !{!19}
!19 = distinct !{!19, !20}
!20 = distinct !{!20, !"LVerDomain"}
!21 = !{!22}
!22 = distinct !{!22, !20}
!23 = distinct !{!23, !7}
!24 = distinct !{!24, !7}
!25 = !{!26}
!26 = distinct !{!26, !27}
!27 = distinct !{!27, !"LVerDomain"}
!28 = !{!29}
!29 = distinct !{!29, !27}
!30 = distinct !{!30, !7}
!31 = distinct !{!31, !7}
!32 = !{!33}
!33 = distinct !{!33, !34}
!34 = distinct !{!34, !"LVerDomain"}
!35 = !{!36}
!36 = distinct !{!36, !34}
!37 = distinct !{!37, !7}
!38 = distinct !{!38, !7}
!39 = !{!40}
!40 = distinct !{!40, !41}
!41 = distinct !{!41, !"LVerDomain"}
!42 = !{!43}
!43 = distinct !{!43, !41}
!44 = distinct !{!44, !7}
!45 = distinct !{!45, !7}
!46 = distinct !{!46, !7}
!47 = distinct !{!47, !7}
!48 = distinct !{!48, !9, !7}
!49 = distinct !{!49, !9, !7}
!50 = distinct !{!50, !7}
!51 = distinct !{!51, !7}
!52 = distinct !{!52, !9, !7}
!53 = distinct !{!53, !9, !7}
!54 = !{i32 -2147036508}
!55 = !{i32 -2147036457}
