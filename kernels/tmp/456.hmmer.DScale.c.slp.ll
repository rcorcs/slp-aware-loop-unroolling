; ModuleID = '456.hmmer.DScale.c'
source_filename = "456.hmmer.DScale.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; Function Attrs: noinline norecurse nounwind uwtable writeonly
define dso_local void @DSet(double* nocapture %vec, i32 %n, double %value) local_unnamed_addr #0 {
entry:
  %cmp4 = icmp sgt i32 %n, 0
  br i1 %cmp4, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  store double %value, double* %arrayidx, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start.p0i8(i64, i8* nocapture) #1

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end.p0i8(i64, i8* nocapture) #1

; Function Attrs: noinline norecurse nounwind uwtable writeonly
define dso_local void @FSet(float* nocapture %vec, i32 %n, float %value) local_unnamed_addr #0 {
entry:
  %cmp4 = icmp sgt i32 %n, 0
  br i1 %cmp4, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  store float %value, float* %arrayidx, align 4, !tbaa !6
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @DScale(double* nocapture %vec, i32 %n, double %scale) local_unnamed_addr #2 {
entry:
  %cmp4 = icmp sgt i32 %n, 0
  br i1 %cmp4, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %mul = fmul double %0, %scale
  store double %mul, double* %arrayidx, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @FScale(float* nocapture %vec, i32 %n, float %scale) local_unnamed_addr #2 {
entry:
  %cmp4 = icmp sgt i32 %n, 0
  br i1 %cmp4, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %mul = fmul float %0, %scale
  store float %mul, float* %arrayidx, align 4, !tbaa !6
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local double @DSum(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp6 = icmp sgt i32 %n, 0
  br i1 %cmp6, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %sum.07 = phi double [ 0.000000e+00, %for.body.preheader ], [ %add, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %add = fadd double %sum.07, %0
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %sum.0.lcssa = phi double [ 0.000000e+00, %entry ], [ %add, %for.body ]
  ret double %sum.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local float @FSum(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp6 = icmp sgt i32 %n, 0
  br i1 %cmp6, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %sum.07 = phi float [ 0.000000e+00, %for.body.preheader ], [ %add, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %add = fadd float %sum.07, %0
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %sum.0.lcssa = phi float [ 0.000000e+00, %entry ], [ %add, %for.body ]
  ret float %sum.0.lcssa
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @DAdd(double* nocapture %vec1, double* nocapture readonly %vec2, i32 %n) local_unnamed_addr #2 {
entry:
  %cmp7 = icmp sgt i32 %n, 0
  br i1 %cmp7, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec2, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %arrayidx2 = getelementptr inbounds double, double* %vec1, i64 %indvars.iv
  %1 = load double, double* %arrayidx2, align 8, !tbaa !2
  %add = fadd double %0, %1
  store double %add, double* %arrayidx2, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @FAdd(float* nocapture %vec1, float* nocapture readonly %vec2, i32 %n) local_unnamed_addr #2 {
entry:
  %cmp7 = icmp sgt i32 %n, 0
  br i1 %cmp7, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec2, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %vec1, i64 %indvars.iv
  %1 = load float, float* %arrayidx2, align 4, !tbaa !6
  %add = fadd float %0, %1
  store float %add, float* %arrayidx2, align 4, !tbaa !6
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @DCopy(double* nocapture %vec1, double* nocapture readonly %vec2, i32 %n) local_unnamed_addr #2 {
entry:
  %cmp7 = icmp sgt i32 %n, 0
  br i1 %cmp7, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec2, i64 %indvars.iv
  %0 = bitcast double* %arrayidx to i64*
  %1 = load i64, i64* %0, align 8, !tbaa !2
  %arrayidx2 = getelementptr inbounds double, double* %vec1, i64 %indvars.iv
  %2 = bitcast double* %arrayidx2 to i64*
  store i64 %1, i64* %2, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @FCopy(float* nocapture %vec1, float* nocapture readonly %vec2, i32 %n) local_unnamed_addr #2 {
entry:
  %cmp7 = icmp sgt i32 %n, 0
  br i1 %cmp7, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec2, i64 %indvars.iv
  %0 = bitcast float* %arrayidx to i32*
  %1 = load i32, i32* %0, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %vec1, i64 %indvars.iv
  %2 = bitcast float* %arrayidx2 to i32*
  store i32 %1, i32* %2, align 4, !tbaa !6
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local double @DDot(double* nocapture readonly %vec1, double* nocapture readonly %vec2, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp9 = icmp sgt i32 %n, 0
  br i1 %cmp9, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %result.010 = phi double [ 0.000000e+00, %for.body.preheader ], [ %add, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec1, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %arrayidx2 = getelementptr inbounds double, double* %vec2, i64 %indvars.iv
  %1 = load double, double* %arrayidx2, align 8, !tbaa !2
  %mul = fmul double %0, %1
  %add = fadd double %result.010, %mul
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %result.0.lcssa = phi double [ 0.000000e+00, %entry ], [ %add, %for.body ]
  ret double %result.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local float @FDot(float* nocapture readonly %vec1, float* nocapture readonly %vec2, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp9 = icmp sgt i32 %n, 0
  br i1 %cmp9, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %result.010 = phi float [ 0.000000e+00, %for.body.preheader ], [ %add, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec1, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %vec2, i64 %indvars.iv
  %1 = load float, float* %arrayidx2, align 4, !tbaa !6
  %mul = fmul float %0, %1
  %add = fadd float %result.010, %mul
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %result.0.lcssa = phi float [ 0.000000e+00, %entry ], [ %add, %for.body ]
  ret float %result.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local double @DMax(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %0 = load double, double* %vec, align 8, !tbaa !2
  %cmp13 = icmp sgt i32 %n, 1
  br i1 %cmp13, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.015 = phi double [ %0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx1 = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %1 = load double, double* %arrayidx1, align 8, !tbaa !2
  %cmp2 = fcmp ogt double %1, %best.015
  %best.1 = select i1 %cmp2, double %1, double %best.015
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi double [ %0, %entry ], [ %best.1, %for.body ]
  ret double %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local float @FMax(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %0 = load float, float* %vec, align 4, !tbaa !6
  %cmp13 = icmp sgt i32 %n, 1
  br i1 %cmp13, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.015 = phi float [ %0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx1 = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %1 = load float, float* %arrayidx1, align 4, !tbaa !6
  %cmp2 = fcmp ogt float %1, %best.015
  %best.1 = select i1 %cmp2, float %1, float %best.015
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi float [ %0, %entry ], [ %best.1, %for.body ]
  ret float %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local double @DMin(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %0 = load double, double* %vec, align 8, !tbaa !2
  %cmp13 = icmp sgt i32 %n, 1
  br i1 %cmp13, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.015 = phi double [ %0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx1 = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %1 = load double, double* %arrayidx1, align 8, !tbaa !2
  %cmp2 = fcmp olt double %1, %best.015
  %best.1 = select i1 %cmp2, double %1, double %best.015
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi double [ %0, %entry ], [ %best.1, %for.body ]
  ret double %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local float @FMin(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %0 = load float, float* %vec, align 4, !tbaa !6
  %cmp13 = icmp sgt i32 %n, 1
  br i1 %cmp13, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.015 = phi float [ %0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx1 = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %1 = load float, float* %arrayidx1, align 4, !tbaa !6
  %cmp2 = fcmp olt float %1, %best.015
  %best.1 = select i1 %cmp2, float %1, float %best.015
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi float [ %0, %entry ], [ %best.1, %for.body ]
  ret float %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local i32 @DArgMax(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp11 = icmp sgt i32 %n, 1
  br i1 %cmp11, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.013 = phi i32 [ 0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %idxprom1 = sext i32 %best.013 to i64
  %arrayidx2 = getelementptr inbounds double, double* %vec, i64 %idxprom1
  %1 = load double, double* %arrayidx2, align 8, !tbaa !2
  %cmp3 = fcmp ogt double %0, %1
  %2 = trunc i64 %indvars.iv to i32
  %best.1 = select i1 %cmp3, i32 %2, i32 %best.013
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi i32 [ 0, %entry ], [ %best.1, %for.body ]
  ret i32 %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local i32 @FArgMax(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp11 = icmp sgt i32 %n, 1
  br i1 %cmp11, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.013 = phi i32 [ 0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %idxprom1 = sext i32 %best.013 to i64
  %arrayidx2 = getelementptr inbounds float, float* %vec, i64 %idxprom1
  %1 = load float, float* %arrayidx2, align 4, !tbaa !6
  %cmp3 = fcmp ogt float %0, %1
  %2 = trunc i64 %indvars.iv to i32
  %best.1 = select i1 %cmp3, i32 %2, i32 %best.013
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi i32 [ 0, %entry ], [ %best.1, %for.body ]
  ret i32 %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local i32 @DArgMin(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp11 = icmp sgt i32 %n, 1
  br i1 %cmp11, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.013 = phi i32 [ 0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %idxprom1 = sext i32 %best.013 to i64
  %arrayidx2 = getelementptr inbounds double, double* %vec, i64 %idxprom1
  %1 = load double, double* %arrayidx2, align 8, !tbaa !2
  %cmp3 = fcmp olt double %0, %1
  %2 = trunc i64 %indvars.iv to i32
  %best.1 = select i1 %cmp3, i32 %2, i32 %best.013
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi i32 [ 0, %entry ], [ %best.1, %for.body ]
  ret i32 %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind readonly uwtable
define dso_local i32 @FArgMin(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #3 {
entry:
  %cmp11 = icmp sgt i32 %n, 1
  br i1 %cmp11, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 1, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %best.013 = phi i32 [ 0, %for.body.preheader ], [ %best.1, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %idxprom1 = sext i32 %best.013 to i64
  %arrayidx2 = getelementptr inbounds float, float* %vec, i64 %idxprom1
  %1 = load float, float* %arrayidx2, align 4, !tbaa !6
  %cmp3 = fcmp olt float %0, %1
  %2 = trunc i64 %indvars.iv to i32
  %best.1 = select i1 %cmp3, i32 %2, i32 %best.013
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  %best.0.lcssa = phi i32 [ 0, %entry ], [ %best.1, %for.body ]
  ret i32 %best.0.lcssa
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @DNorm(double* nocapture %vec, i32 %n) local_unnamed_addr #2 {
entry:
  %call = tail call double @DSum(double* %vec, i32 %n)
  %cmp = fcmp une double %call, 0.000000e+00
  %cmp125 = icmp sgt i32 %n, 0
  br i1 %cmp, label %for.cond.preheader, label %for.cond2.preheader

for.cond2.preheader:                              ; preds = %entry
  br i1 %cmp125, label %for.body4.lr.ph, label %if.end

for.body4.lr.ph:                                  ; preds = %for.cond2.preheader
  %conv = sitofp i32 %n to double
  %div5 = fdiv double 1.000000e+00, %conv
  %wide.trip.count32 = zext i32 %n to i64
  br label %for.body4

for.cond.preheader:                               ; preds = %entry
  br i1 %cmp125, label %for.body.preheader, label %if.end

for.body.preheader:                               ; preds = %for.cond.preheader
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %div = fdiv double %0, %call
  store double %div, double* %arrayidx, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %if.end, label %for.body

for.body4:                                        ; preds = %for.body4, %for.body4.lr.ph
  %indvars.iv30 = phi i64 [ 0, %for.body4.lr.ph ], [ %indvars.iv.next31, %for.body4 ]
  %arrayidx7 = getelementptr inbounds double, double* %vec, i64 %indvars.iv30
  store double %div5, double* %arrayidx7, align 8, !tbaa !2
  %indvars.iv.next31 = add nuw nsw i64 %indvars.iv30, 1
  %exitcond33 = icmp eq i64 %indvars.iv.next31, %wide.trip.count32
  br i1 %exitcond33, label %if.end, label %for.body4

if.end:                                           ; preds = %for.body4, %for.body, %for.cond2.preheader, %for.cond.preheader
  ret void
}

; Function Attrs: noinline norecurse nounwind uwtable
define dso_local void @FNorm(float* nocapture %vec, i32 %n) local_unnamed_addr #2 {
entry:
  %call = tail call float @FSum(float* %vec, i32 %n)
  %cmp = fcmp une float %call, 0.000000e+00
  %cmp231 = icmp sgt i32 %n, 0
  br i1 %cmp, label %for.cond.preheader, label %for.cond4.preheader

for.cond4.preheader:                              ; preds = %entry
  br i1 %cmp231, label %for.body7.lr.ph, label %if.end

for.body7.lr.ph:                                  ; preds = %for.cond4.preheader
  %conv8 = sitofp i32 %n to float
  %conv11 = fdiv float 1.000000e+00, %conv8
  %wide.trip.count38 = zext i32 %n to i64
  br label %for.body7

for.cond.preheader:                               ; preds = %entry
  br i1 %cmp231, label %for.body.preheader, label %if.end

for.body.preheader:                               ; preds = %for.cond.preheader
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %div = fdiv float %0, %call
  store float %div, float* %arrayidx, align 4, !tbaa !6
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %if.end, label %for.body

for.body7:                                        ; preds = %for.body7, %for.body7.lr.ph
  %indvars.iv36 = phi i64 [ 0, %for.body7.lr.ph ], [ %indvars.iv.next37, %for.body7 ]
  %arrayidx13 = getelementptr inbounds float, float* %vec, i64 %indvars.iv36
  store float %conv11, float* %arrayidx13, align 4, !tbaa !6
  %indvars.iv.next37 = add nuw nsw i64 %indvars.iv36, 1
  %exitcond39 = icmp eq i64 %indvars.iv.next37, %wide.trip.count38
  br i1 %exitcond39, label %if.end, label %for.body7

if.end:                                           ; preds = %for.body7, %for.body, %for.cond4.preheader, %for.cond.preheader
  ret void
}

; Function Attrs: noinline nounwind uwtable
define dso_local void @DLog(double* nocapture %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %cmp15 = icmp sgt i32 %n, 0
  br i1 %cmp15, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.inc, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.inc ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %cmp1 = fcmp ogt double %0, 0.000000e+00
  br i1 %cmp1, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %call = tail call double @log(double %0) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then
  %storemerge = phi double [ %call, %if.then ], [ 0xFFEFFFFFFFFFFFFF, %for.body ]
  store double %storemerge, double* %arrayidx, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.inc, %entry
  ret void
}

; Function Attrs: nounwind
declare dso_local double @log(double) local_unnamed_addr #5

; Function Attrs: noinline nounwind uwtable
define dso_local void @FLog(float* nocapture %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %cmp18 = icmp sgt i32 %n, 0
  br i1 %cmp18, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.inc, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.inc ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %cmp1 = fcmp ogt float %0, 0.000000e+00
  br i1 %cmp1, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %conv = fpext float %0 to double
  %call = tail call double @log(double %conv) #6
  %conv6 = fptrunc double %call to float
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then
  %storemerge = phi float [ %conv6, %if.then ], [ 0xC7EFFFFFE0000000, %for.body ]
  store float %storemerge, float* %arrayidx, align 4, !tbaa !6
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.inc, %entry
  ret void
}

; Function Attrs: noinline nounwind uwtable
define dso_local void @DExp(double* nocapture %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %cmp8 = icmp sgt i32 %n, 0
  br i1 %cmp8, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %call = tail call double @exp(double %0) #6
  store double %call, double* %arrayidx, align 8, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: nounwind
declare dso_local double @exp(double) local_unnamed_addr #5

; Function Attrs: noinline nounwind uwtable
define dso_local void @FExp(float* nocapture %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %cmp9 = icmp sgt i32 %n, 0
  br i1 %cmp9, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.body, %for.body.preheader
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %conv = fpext float %0 to double
  %call = tail call double @exp(double %conv) #6
  %conv1 = fptrunc double %call to float
  store float %conv1, float* %arrayidx, align 4, !tbaa !6
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.body, %entry
  ret void
}

; Function Attrs: noinline nounwind uwtable
define dso_local double @DLogSum(double* nocapture readonly %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %call = tail call double @DMax(double* %vec, i32 %n)
  %cmp21 = icmp sgt i32 %n, 0
  br i1 %cmp21, label %for.body.lr.ph, label %for.end

for.body.lr.ph:                                   ; preds = %entry
  %sub = fadd double %call, -5.000000e+01
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.inc, %for.body.lr.ph
  %indvars.iv = phi i64 [ 0, %for.body.lr.ph ], [ %indvars.iv.next, %for.inc ]
  %sum.023 = phi double [ 0.000000e+00, %for.body.lr.ph ], [ %sum.1, %for.inc ]
  %arrayidx = getelementptr inbounds double, double* %vec, i64 %indvars.iv
  %0 = load double, double* %arrayidx, align 8, !tbaa !2
  %cmp1 = fcmp ogt double %0, %sub
  br i1 %cmp1, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %sub4 = fsub double %0, %call
  %call5 = tail call double @exp(double %sub4) #6
  %add = fadd double %sum.023, %call5
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then
  %sum.1 = phi double [ %add, %if.then ], [ %sum.023, %for.body ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.inc, %entry
  %sum.0.lcssa = phi double [ 0.000000e+00, %entry ], [ %sum.1, %for.inc ]
  %call6 = tail call double @log(double %sum.0.lcssa) #6
  %add7 = fadd double %call, %call6
  ret double %add7
}

; Function Attrs: noinline nounwind uwtable
define dso_local float @FLogSum(float* nocapture readonly %vec, i32 %n) local_unnamed_addr #4 {
entry:
  %call = tail call float @FMax(float* %vec, i32 %n)
  %cmp29 = icmp sgt i32 %n, 0
  %conv1 = fpext float %call to double
  br i1 %cmp29, label %for.body.lr.ph, label %for.end

for.body.lr.ph:                                   ; preds = %entry
  %sub = fadd double %conv1, -5.000000e+01
  %wide.trip.count = zext i32 %n to i64
  br label %for.body

for.body:                                         ; preds = %for.inc, %for.body.lr.ph
  %indvars.iv = phi i64 [ 0, %for.body.lr.ph ], [ %indvars.iv.next, %for.inc ]
  %sum.031 = phi float [ 0.000000e+00, %for.body.lr.ph ], [ %sum.1, %for.inc ]
  %arrayidx = getelementptr inbounds float, float* %vec, i64 %indvars.iv
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %conv = fpext float %0 to double
  %cmp2 = fcmp olt double %sub, %conv
  br i1 %cmp2, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %sub6 = fsub float %0, %call
  %conv7 = fpext float %sub6 to double
  %call8 = tail call double @exp(double %conv7) #6
  %conv9 = fpext float %sum.031 to double
  %add = fadd double %call8, %conv9
  %conv10 = fptrunc double %add to float
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then
  %sum.1 = phi float [ %conv10, %if.then ], [ %sum.031, %for.body ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond, label %for.end, label %for.body

for.end:                                          ; preds = %for.inc, %entry
  %sum.0.lcssa = phi float [ 0.000000e+00, %entry ], [ %sum.1, %for.inc ]
  %conv11 = fpext float %sum.0.lcssa to double
  %call12 = tail call double @log(double %conv11) #6
  %add14 = fadd double %call12, %conv1
  %conv15 = fptrunc double %add14 to float
  ret float %conv15
}

; Function Attrs: noinline nounwind uwtable
define dso_local i32 @main() local_unnamed_addr #4 {
entry:
  %Ans = alloca double, align 8
  %vla3 = alloca [2048 x double], align 16
  %vla3.sub = getelementptr inbounds [2048 x double], [2048 x double]* %vla3, i64 0, i64 0
  call void asm sideeffect "", "imr,~{memory},~{dirflag},~{fpsr},~{flags}"(double* nonnull %vla3.sub) #6, !srcloc !8
  %0 = bitcast double* %Ans to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* nonnull %0) #6
  %call = call double @DSum(double* nonnull %vla3.sub, i32 2048)
  store double %call, double* %Ans, align 8, !tbaa !2
  call void asm sideeffect "", "imr,~{memory},~{dirflag},~{fpsr},~{flags}"(double* nonnull %Ans) #6, !srcloc !9
  call void @llvm.lifetime.end.p0i8(i64 8, i8* nonnull %0) #6
  ret i32 0
}

attributes #0 = { noinline norecurse nounwind uwtable writeonly "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="skylake" "target-features"="+adx,+aes,+avx,+avx2,+bmi,+bmi2,+clflushopt,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+invpcid,+lzcnt,+mmx,+movbe,+mpx,+pclmul,+popcnt,+prfchw,+rdrnd,+rdseed,+rtm,+sahf,+sgx,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsavec,+xsaveopt,+xsaves" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind }
attributes #2 = { noinline norecurse nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="skylake" "target-features"="+adx,+aes,+avx,+avx2,+bmi,+bmi2,+clflushopt,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+invpcid,+lzcnt,+mmx,+movbe,+mpx,+pclmul,+popcnt,+prfchw,+rdrnd,+rdseed,+rtm,+sahf,+sgx,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsavec,+xsaveopt,+xsaves" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noinline norecurse nounwind readonly uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="skylake" "target-features"="+adx,+aes,+avx,+avx2,+bmi,+bmi2,+clflushopt,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+invpcid,+lzcnt,+mmx,+movbe,+mpx,+pclmul,+popcnt,+prfchw,+rdrnd,+rdseed,+rtm,+sahf,+sgx,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsavec,+xsaveopt,+xsaves" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="skylake" "target-features"="+adx,+aes,+avx,+avx2,+bmi,+bmi2,+clflushopt,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+invpcid,+lzcnt,+mmx,+movbe,+mpx,+pclmul,+popcnt,+prfchw,+rdrnd,+rdseed,+rtm,+sahf,+sgx,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsavec,+xsaveopt,+xsaves" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="skylake" "target-features"="+adx,+aes,+avx,+avx2,+bmi,+bmi2,+clflushopt,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+invpcid,+lzcnt,+mmx,+movbe,+mpx,+pclmul,+popcnt,+prfchw,+rdrnd,+rdseed,+rtm,+sahf,+sgx,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsavec,+xsaveopt,+xsaves" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 8.0.0 (trunk)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"double", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"float", !4, i64 0}
!8 = !{i32 -2147036508}
!9 = !{i32 -2147036457}
