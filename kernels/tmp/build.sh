LLVM_PATH=/home/rodrigo/tmp/build/bin/

OPT="-O3 -march=skylake -mtune=skylake"
#OPT="-O3 -march=native"
#OPT="-O3"
CFLAG=-lm
SLPSETTINGS="-mllvm -slp-min-tree-size=2"

${LLVM_PATH}clang $1 ${OPT} -fno-vectorize -fslp-vectorize -fno-unroll-loops -fno-inline-functions -mllvm -slp-unroll-loops=false ${SLPSETTINGS} -emit-llvm -S -o $(basename $1).slp.ll 1>/dev/null 2&>1
${LLVM_PATH}clang $1 ${OPT} -fno-vectorize -fslp-vectorize -fno-unroll-loops -fno-inline-functions -mllvm -slp-unroll-loops=true -mllvm -slp-unroll-runtime=true ${SLPSETTINGS} -emit-llvm -S -o $(basename $1).luslp.ll 1>/dev/null 2&>1
${LLVM_PATH}clang $1 ${OPT} -fvectorize -fno-slp-vectorize -fno-unroll-loops -fno-inline-functions -emit-llvm -S -o $(basename $1).vec.ll 1>/dev/null 2&>1


${LLVM_PATH}clang $1 ${OPT} -fno-vectorize -fno-slp-vectorize -fno-unroll-loops -fno-inline-functions -o $(basename $1).baseline ${CFLAG}
${LLVM_PATH}clang $1 ${OPT} -fno-vectorize -fslp-vectorize -fno-unroll-loops -fno-inline-functions -mllvm -slp-unroll-loops=false ${SLPSETTINGS} -o $(basename $1).slp ${CFLAG}
${LLVM_PATH}clang $1 ${OPT} -fno-vectorize -fslp-vectorize -fno-unroll-loops -fno-inline-functions -mllvm -slp-unroll-loops=true -mllvm -slp-unroll-runtime=true ${SLPSETTINGS} -o $(basename $1).luslp ${CFLAG}
${LLVM_PATH}clang $1 ${OPT} -fvectorize -fno-slp-vectorize -fno-unroll-loops -fno-inline-functions -o $(basename $1).vec ${CFLAG}

