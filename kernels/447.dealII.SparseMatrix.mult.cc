
#include <cstdio>

#include "447.dealII/sparse_matrix.h"
#include "447.dealII/sparse_matrix.templates.h"

#include "common.h"

#define NRep 10000
int main() {
  int n = 1024;
  __escape__(&n);
  SparseMatrix<double> Mat(SparsityPattern(n,n,n/2));
  #pragma nounroll
  for(int repeat = 0; repeat<NRep; repeat++) {
    double Num = 2.71828;
    __escape__(&Num);
    __escape__(&Mat);
    Mat *= Num;
    __escape__(&Mat);
  }
  return 0;
}
