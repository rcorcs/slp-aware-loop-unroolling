/* vectorops.c
 * Operations on vectors of floats or doubles.
 * 
 * DSet(), FSet()       - set all items in vector to value.
 * DScale(), FScale()   - multiply all items in vector by scale
 * DSum(), FSum()       - return sum of values in vector
 * DAdd(), FAdd()       - add vec2 to vec1.
 * DCopy(), FCopy()     - set vec1 to be same as vec2. 
 * DDot(), FDot()       - return dot product of two vectors.
 * DMax(), FMax()       - return value of maximum element in vector
 * DMin(), FMin()       - return value of minimum element in vector 
 * DArgMax(), FArgMax() - return index of maximum element in vector
 * DArgMin(), FArgMin() - return index of minimum element in vector
 * 
 * DNorm(), FNorm()     - normalize a probability vector of length n.
 * DLog(), FLog()       - convert to log probabilities 
 * DExp(), FExp()       - convert log p's back to probabilities
 * DLogSum(), FLogSum() - given vector of log p's; return log of summed p's.
 *                        
 * SRE, Tue Oct  1 15:23:25 2002 [St. Louis]
 * CVS $Id: vectorops.c,v 1.4 2003/04/14 16:00:16 eddy Exp $                       
 */                      
  

#include <stdlib.h>
#include <math.h>
#include <float.h>

#include "common.h"

void
DSet(double *vec, int n, double value)
{
  int x; 
  for (x = 0; x < n; x++) vec[x] = value;
}

#define NRep 10000
int main() {
  int n = 262144;
  __escape__(&n);
  double vec[n];
  #pragma nounroll
  for(int repeat = 0; repeat<NRep; repeat++) {
    double value = 3.1415;
    __escape__(vec);
    __escape__(&value);
    DSet(vec, n, value); 
    __escape__(vec);
  }
  return 0;
}

