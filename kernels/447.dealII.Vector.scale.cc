
#include <cstdio>

#include "447.dealII/vector.h"
#include "447.dealII/vector.templates.h"

#include "common.h"

#define NRep 10000
int main() {
  int n = 262144;
  __escape__(&n);
  Vector<float> Vec(n);
  Vec = 3.1415f;
  #pragma nounroll
  for(int repeat = 0; repeat<NRep; repeat++) {
    float Num = 2.71828f;
    __escape__(&Num);
    __escape__(&Vec);
    Vec.scale(Num);
    __escape__(&Vec);
  }
  return 0;
}
